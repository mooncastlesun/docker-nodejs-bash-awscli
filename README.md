# docker-aws

BitBucket PipelinesでS3へデプロイする時に使用するDockerイメージ

# 環境
* Docker
  Docker for MacOSをインストール&起動して下さい => [Docker for Mac](https://docs.docker.com/docker-for-mac/install/)
* make
```shell
$ brew install make
```

# Dockerイメージの内容
* aws-cli (awsコマンド)
* python2-boto2
* bash
* curl
* findutils
* git
* libmagic (s3-parallel-putの依存ライブラリ)
* openssh (ssh, scp, sftp)
* python2
* rsync
* s3-parallel-put
* tree

イメージのエントリーポイントはBash。

# Dockerイメージのビルド
```shell
$ make
```

# DockerイメージをAmazon ECRへプッシュ
注意) プッシュするにはAWS ECRの「リポジトリへのプッシュ権限」が必要です。管理者に相談して付与してもらってください。

## awsコマンド用の認証情報など設定
```shell
# インストール
$ brew install awscli

# 設定
$ aws configure
# 例)
AWS Access Key ID [None]: AKIAIOSFODNN7EXAMPLE
AWS Secret Access Key [None]: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
Default region name [None]: ap-northeast-1
Default output format [None]: json
```

## プッシュ
```shell
$ make push
```
## Dockerイメージを変更した時
* Makefile内のバージョン文字列をインクリメント
* ```make```
* ```make push```

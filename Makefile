IMAGE=dseg/nodejs-bash-awscli
VERSION=0.0.2

default: run

run: build

push:
	docker push $(IMAGE):$(VERSION)
	docker push $(IMAGE):latest

build: Dockerfile
	docker build -t $(IMAGE):$(VERSION) .
	docker tag $(IMAGE):$(VERSION) $(IMAGE):latest

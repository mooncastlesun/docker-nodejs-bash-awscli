FROM mesosphere/aws-cli:1.14.5
RUN \
 apk -v --update add \
  bash \
  libmagic \
  rsync \
  py-pip \
  nodejs \
  nodejs-npm \
 && \
 pip install --upgrade python-magic boto boto3 && \
 npm i -g yarn && \
 apk -v --purge del py-pip && \
 rm /var/cache/apk/*
ENTRYPOINT ["/bin/bash"]
